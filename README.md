# INTRODUCTION
>It takes many years to learn to paint, to draw, or to sculpt, but the programming aptitude required to get professional results in generative art can be learnt in a matter of days.
>-Matt Pearson

Programming is a fascinating thing. It is a wide field. In this project, I have decided to take an artistic and creative approach to it. I seek to generate art using code. I'll be explaining some of my code at my [blog](www.saitamasensei.angstrom.com).

# Getting Started
These instructions can be found at the main [processing](https://processing.org/tutorials/gettingstarted/) website.
1. On Windows, you'll have a .zip file. Double-click it, and drag the folder inside to a location on your hard disk. It could be Program Files or simply the desktop, but the important thing is for the processing folder to be pulled out of that .zip file. Then double-click processing.exe to start.
2. The Mac OS X version is also a .zip file. Double-click it and drag the Processing icon to the Applications folder. If you're using someone else's machine and can't modify the Applications folder, just drag the application to the desktop. Then double-click the Processing icon to start.
3. The Linux version is a .tar.gz file, which should be familiar to most Linux users. Download the file to your home directory, then open a terminal window, and type: `tar xvfz processing-xxxx.tgz` (Replace xxxx with the rest of the file's name, which is the version number.) This will create a folder named processing-2.0 or something similar. Then change to that directory: `cd processing-xxxx` and run it: `./processing`
