fullScreen();
background(255);
strokeWeight(.5);
smooth();


int centX = width/2;
int centY = height/2;

float x, y;

for(int i = 0; i < 100; i++){
  float lastx = -999;
  float lasty = -999;
  float radius = 5;
  float radiusNoise = random(10);
  stroke(random(20), random(50), random(70), 80);
  int startangle = int(random(360));
  int endangle = 3600+ int(random(3600));
  int anglestep = 5 + int(random(3));
  
  for(float ang = startangle; ang<=endangle; ang+=anglestep){
     radiusNoise += .05;
     radius += .5;
     float rad = radians(ang);
     float thisRadius = radius + (noise(radiusNoise) *200) - 100;
     x = centX + (thisRadius * cos(rad));
     y = centY + (thisRadius * sin(rad));
     if(lastx > -999){
       line(x,y,lastx,lasty);
     }
     lastx = x;
     lasty = y;
}
}
save("fractalnoise.png");